package fr.formation.servlet;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.dao.IProduitDao;
import fr.formation.dao.sql.ProduitDaoSql;
import fr.formation.model.Fournisseur;
import fr.formation.model.Produit;

//MAPPING (URL D'ACCES) !!!!
@WebServlet("/produit-ajout")
public class ProduitAjoutServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// On délègue directement vers la vue
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/produit-ajout.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Récuparation des paramètres du produit (via le formulaire)
		String nomProduit = req.getParameter("libelle");
		String prixProduitString = req.getParameter("prix");
		String stockProduitString = req.getParameter("stock");

		BigDecimal prixProduit = new BigDecimal(prixProduitString);
		int stockProduit = Integer.parseInt(stockProduitString);

		Fournisseur monFournisseur = new Fournisseur();
		monFournisseur.setId(1);

		// Création du produit avec le nom du produit
		Produit monProduit = new Produit(nomProduit, prixProduit);

		monProduit.setPrix(prixProduit);
		monProduit.setStock(stockProduit);
		monProduit.setFournisseur(monFournisseur);

		// Ajoute le produit
		IProduitDao daoProduit = new ProduitDaoSql();

		daoProduit.add(monProduit);

		// Redirection vers la page des produits
		resp.sendRedirect("produits");
	}
}