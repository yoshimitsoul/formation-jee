package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.dao.ICategorieDao;
import fr.formation.dao.sql.CategorieDaoSql;

@WebServlet
public class CategorieServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ICategorieDao daoCategorie = new CategorieDaoSql();

		req.setAttribute("categories", daoCategorie.findAll());

		this.getServletContext().getRequestDispatcher("/WEB-INF/views/categorie.jsp");
	}

}
