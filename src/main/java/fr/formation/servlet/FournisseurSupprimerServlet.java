package fr.formation.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.model.Fournisseur;

@WebServlet("/fournisseur-supprimer")
public class FournisseurSupprimerServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String idString = req.getParameter("id");
		int id = Integer.parseInt(idString);

		List<Fournisseur> mesFournisseurs = (List<Fournisseur>) this.getServletContext().getAttribute("fournisseurs");
		mesFournisseurs.remove(id);

		resp.sendRedirect("fournisseurs");
	}

}
