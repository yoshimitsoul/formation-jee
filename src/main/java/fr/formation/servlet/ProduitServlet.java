package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.dao.IProduitDao;
import fr.formation.dao.sql.ProduitDaoSql;

@WebServlet("/produits")
public class ProduitServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		IProduitDao daoProduit = new ProduitDaoSql();

		req.setAttribute("produits", daoProduit.findAll());

		// DELEGUATION DE LA REQUETE
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/produits.jsp").forward(req, resp);
	}
}