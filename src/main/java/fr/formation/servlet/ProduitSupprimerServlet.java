package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.dao.IProduitDao;
import fr.formation.dao.sql.ProduitDaoSql;

@WebServlet("/produit-supprimer")
public class ProduitSupprimerServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// recuperation de l'index du produit à supprimer
		String idString = req.getParameter("numSS");
		int id = Integer.parseInt(idString);

		// supprimer le produit de la liste
		IProduitDao daoProduit = new ProduitDaoSql();

		daoProduit.deleteById(id);

		// redirection sur liste des produits
		resp.sendRedirect("produits");

	}
}
