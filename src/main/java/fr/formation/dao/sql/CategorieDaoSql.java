package fr.formation.dao.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.formation.dao.ICategorieDao;
import fr.formation.model.Categorie;

public class CategorieDaoSql extends DaoSql implements ICategorieDao {

	@Override
	public List<Categorie> findAll() {
		List<Categorie> mesCategories = new ArrayList<>();

		try {
			this.openConnection();

			Statement monStatement = this.connexionSql.createStatement();
			ResultSet monResultat = monStatement.executeQuery("SELECT * FROM categorie");

			while (monResultat.next()) {
				Categorie maCategorie = new Categorie();

				maCategorie.setId(monResultat.getInt("PRO_ID"));
				maCategorie.setLibelle(monResultat.getString("PRO_LIBELLE"));

				mesCategories.add(maCategorie);

			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		finally {
			this.closeConnection();
		}

		return mesCategories;
	}

	@Override
	public Categorie findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Categorie add(Categorie entity) {
		try {
			this.openConnection();

			PreparedStatement monStatementInsert = this.connexionSql
					.prepareStatement("INSERT INTO categorie (CAT_ID, CAT_LIBELLE, CAT_PARENT_ID) VALUES (?, ?, ?)");

			// REMPLIR LES ? (Paramétres indexés)
			monStatementInsert.setInt(1, entity.getId());
			monStatementInsert.setString(2, entity.getLibelle());
			monStatementInsert.setString(3, entity.getParent());

			monStatementInsert.execute();
		}

		catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		finally {
			this.closeConnection();
		}

		return entity;
	}

	@Override
	public Categorie save(Categorie entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteById(int id) {
		try {
			this.openConnection();

			PreparedStatement monStatementInsert = this.connexionSql
					.prepareStatement("DELETE FROM categorie WHERE CAT_ID = ?");

			monStatementInsert.setInt(1, id);

			monStatementInsert.execute();

			return true;
		}

		catch (SQLException sqle) {
			sqle.printStackTrace();
			return false;
		}

		finally {
			this.closeConnection();
		}
	}
}
