package fr.formation.dao.jpa;

import java.util.List;

import fr.formation.dao.IProduitDao;
import fr.formation.model.Produit;

public class ProduitDaoJpa extends DaoJpa implements IProduitDao {

	@Override
	public List<Produit> findAll() {
		// Lister toutes les Catégories
		List<Produit> produits = em.createQuery("select p from Produit p", Produit.class).getResultList();

		return produits;
	}

	@Override
	public Produit findById(int id) {
		return em.find(Produit.class, id);
	}

	@Override
	public Produit add(Produit entity) {
		em.getTransaction().begin();
		em.persist(entity);
		em.getTransaction().commit();

		return entity;
	}

	@Override
	public Produit save(Produit entity) {
		em.getTransaction().begin();
		entity = em.merge(entity);
		em.getTransaction().commit();

		return entity;
	}

	@Override
	public boolean deleteById(int id) {
		em.getTransaction().begin();
		em.createQuery("delete from Produit p where p.id = ?1").setParameter(1, id).executeUpdate();
		em.getTransaction().commit();

		return true;
	}

}
